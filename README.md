## 介绍

- MAKU 微信小程序端，采用微信原生小程序开发，使用门槛极低！
- 后端地址：https://gitee.com/makunet/maku-boot
- 开发文档：https://maku.net/docs/maku-boot
- 官网地址：https://maku.net

## 小程序演示

<img src="images/code.jpg"/>

## 安装

注意：需使用 nodejs 长期维护版本，如：[18.x、20.x]，能保证项目的稳定运行。

```bash
# 克隆项目
git clone https://gitee.com/makunet/maku-app.git

# 进入项目
cd maku-app

# 安装依赖
npm install

# 导入到微信开发者工具
工具 -> 构建 npm
```

## 微信交流群

为了更好的交流，我们新提供了微信交流群，需扫描下面的二维码，关注公众号，回复【加群】，根据提示信息，作者会拉你进群的，感谢配合！

![](https://maku.net/app/img/qrcode.jpg)

## 开源汇总

- 低代码开发平台（单体版）：https://gitee.com/makunet/maku-boot
- 低代码开发平台（微服务）：https://gitee.com/makunet/maku-cloud
- 超好用的代码生成器：https://gitee.com/makunet/maku-generator
- Vue3.x 后台管理 UI：https://gitee.com/makunet/maku-admin
- Vue3.x 表单设计器：https://gitee.com/makunet/maku-form-design

## 效果图

<table>
    <tr>
        <td><img src="images/1.jpg"/></td>
        <td><img src="images/2.jpg"/></td>
    </tr>
    <tr>
        <td><img src="images/3.jpg"/></td>
        <td><img src="images/4.jpg"/></td>
    </tr>
</table>

## 版权说明

- MAKU 生态技术框架全系列版本遵循 Apache License 2.0 开源协议。
- 允许个人项目、承接私活及企业内部项目使用，可享有终身免费许可。
- 禁止任何针对开源项目或产品销售的二次开发行为，未经授权的行为将视为侵权。
- 请尊重知识产权，不允许删除源码注释申明及作者信息，违者将追究法律责任。
